window.addEventListener('load', function() {
  options.isActivated.checked = localStorage.isActivated;
  options.accountName.value = localStorage.accountName;
  options.accountPassword.value = localStorage.accountPassword;
  options.repositoryOwner.value = localStorage.repositoryOwner;
  options.repositoryName.value = localStorage.repositoryName;
  
  options.isActivated.onchange = function() {
	localStorage.isActivated = options.isActivated.checked;
  };
  
  options.save.onclick = function() {
	localStorage.isActivated = options.isActivated.checked;
	localStorage.accountName = options.accountName.value;
	localStorage.accountPassword = options.accountPassword.value;
	localStorage.repositoryOwner = options.repositoryOwner.value;
	localStorage.repositoryName = options.repositoryName.value;
	alert('Saved!');
  };
});